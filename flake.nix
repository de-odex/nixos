{
  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-unstable";
    };
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, sops-nix, ... }@inputs: {
    nixosConfigurations = {
      konoe = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        #specialArgs = inputs;
        modules = [
          sops-nix.nixosModules.sops
          # TODO: move imports maybe lol
          ./projects/proj1.nix
          ./projects/proj/konoe.nix
          ./machine-types/linode.nix
          ./modules/default.nix
          ./common.nix
        ];
      };
    };
  };
}
