{ config, pkgs, ... }:
{
  nix = {
    package = pkgs.nixFlakes;
    gc = {
      automatic = true;
      options = "--delete-older-than 7d";
    };
    optimise.automatic = true;
    extraOptions = ''
      extra-experimental-features = nix-command flakes
    '';
  };
  system.autoUpgrade = {
    enable = true;
    allowReboot = true;
    flake = "path:/etc/nixos/";
  };


  ## Use the GRUB 2 boot loader.
  #boot.loader.grub.enable = true;
  #boot.loader.grub.version = 2;
  ##boot.loader.grub.efiSupport = true;
  ## boot.loader.grub.efiInstallAsRemovable = true;
  ##boot.loader.efi.efiSysMountPoint = "/boot/efi";

  networking.useDHCP = false;

  networking.firewall.enable = true;
  networking.firewall.allowedTCPPorts = [ 22 ];
  networking.firewall.allowedUDPPorts = [  ];


  time.timeZone = "Asia/Tokyo";
  #i18n.defaultLocale = "en_US.UTF-8";
  #i18n.extraLocaleSettings = {
  #  LC_CTYPE = "en_US.UTF-8";
  #  LC_NUMERIC = "ja_JP.UTF-8";
  #  LC_TIME = "ja_JP.UTF-8";
  #  LC_COLLATE = "en_US.UTF-8";
  #  LC_MONETARY = "ja_JP.UTF-8";
  #  LC_MESSAGES = "en_US.UTF-8";
  #  LC_PAPER = "ja_JP.UTF-8";
  #  LC_NAME = "ja_JP.UTF-8";
  #  LC_ADDRESS = "ja_JP.UTF-8";
  #  LC_TELEPHONE = "ja_JP.UTF-8";
  #  LC_MEASUREMENT = "ja_JP.UTF-8";
  #  LC_IDENTIFICATION = "ja_JP.UTF-8";
  #};
  #console = {
  #  font = "Lat2-Terminus16";
  #  keyMap = "us";
  #};


  programs = {
    fish = {
      enable = true;
    };
    neovim = {
      enable = true;
      defaultEditor = true;
      viAlias = true;
      vimAlias = true;
    };
    git = {
      enable = true;
      config = { init.defaultBranch = "master"; };
    };
  };

  services = {
    openssh = {
      enable = true;
      hostKeys = [
        {
          path = "/etc/ssh/ssh_host_ed25519_key";
          type = "ed25519";
        }
      ];
    };
  };
}
