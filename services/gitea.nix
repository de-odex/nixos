{ config, lib, pkgs, ... }:
{
  containers.database = {
    config =
      { config, pkgs, ... }:
      {
        networking.firewall.enable = false;

        services.gitea.enable = true;
        services.gitea.disableRegistration = true;
        services.gitea.settings = {
          mailer.ENABLED = false;
        };

        # The Gitea module does not allow adding users declaratively
        systemd.services.gitea-add-user = {
          description = "gitea-add-user";
          wants = [ "gitea.service" ];
          wantedBy = [ "multi-user.target" ];
          path = [ pkgs.gitea ];
          script = ''
            ${pkgs.gitea}/bin/gitea admin create-user --admin --username deodex --password totallysafe --email deodex@localhost &&
            ${pkgs.gitea}/bin/gitea admin create-user --admin --username eva --password totallysafe --email eva@localhost &&
            ${pkgs.gitea}/bin/gitea admin create-user --admin --username leeson --password totallysafe --email leeson@localhost
          '';
          serviceConfig = {
            Restart = "always";
            User = "gitea";
            Group = "gitea";
            WorkingDirectory = config.services.gitea.stateDir;
          };
          environment = { GITEA_WORK_DIR = config.services.gitea.stateDir; };
        };

        environment.systemPackages = [ pkgs.gitea pkgs.neovim ];

        services.openssh.enable = true;

        users.users.root.initialHashedPassword = "";
        users.mutableUsers = false;
      };
  };
}
