{ pkgs, ... }:
{
  users.users = {
    root = {
      passwordFile = config.sops.secrets."users/root".path;
      openssh.authorizedKeys.keys = [
        # there's a key here
      ];
    };

    deodex = {
      isNormalUser = true;
      extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
      shell = pkgs.fish;
      passwordFile = config.sops.secrets."users/deodex".path;
      openssh.authorizedKeys.keys = [
        # there's a key here
        # i know theyre safe to commit
        # i just removed it because xd
      ];
    };
  };

  programs = {
    zsh = {
      enable = true;
    };
  };
}
