{ config, pkgs, ... }:
{
  deodex = {
    hardening.enable = true;
    paranoia = {
      enable = true;
      persistMount = {
        device = "/dev/sda";
        fsType = "ext4";
      };
      rootMount = {
        device = "/dev/sdc";
        fsType = "ext4";
        options = [ "noexec" ];
      };
    };
  };

  sops = {
    defaultSopsFile = ../../secrets/konoe.yaml;
    # is this safe??
    age.keyFile = "/mnt/persist/etc/nixos/secrets/konoe.age.private";
    secrets = {
      "users/root" = {
        neededForUsers = true;
      };
      "users/deodex" = {
        neededForUsers = true;
      };
    };
  };

  networking.hostName = "konoe";

  system.stateVersion = "22.05";
}
