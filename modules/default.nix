{ ... }:
{
  imports = [
    ./harden.nix
    ./paranoia.nix
  ];
}
