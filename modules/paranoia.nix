# TODO: more options (persistMount, regularReboot)
{ config, pkgs, lib, ... }:
with lib;
let
  cfg = config.deodex;
in
  {
    options.deodex.paranoia = {
      enable = mkEnableOption "Enable paranoid settings.";
      persistMount = mkOption {
        type = types.attrsOf types.anything;
        example = {
          device = "/dev/disk/by-label/nixos-persist";
          fsType = "ext4";
        };
        description = "The disk to mount to /mnt/persist.";
      };
      # NOTE: temporary requirement for as long
      #       as i cannot use tmpfs for root
      rootMount = mkOption {
        type = types.attrsOf types.anything;
        example = {
          device = "/dev/disk/by-label/nixos-root";
          fsType = "ext4";
        };
        description = "The disk to mount to /.";
      };
    };


    config = mkIf cfg.paranoia.enable {
      systemd = {
        services = {
          regularReboot = {
             serviceConfig = {
              Type = "simple";
              ExecStart = "${pkgs.systemd}/bin/systemctl --no-block reboot";
            };
          };
        };
        timers = {
          regularReboot = {
            timerConfig = {
              OnBootSec = "23h 30m";
              RandomizedDelaySec = "1h";
              FixedRandomDelay = "true";
            };
            wantedBy = [ "default.target" ];
          };
        };
      };

      environment.etc = {
        "ssh/ssh_host_rsa_key".source = "/mnt/persist/etc/ssh/ssh_host_rsa_key";
        "ssh/ssh_host_rsa_key.pub".source = "/mnt/persist/etc/ssh/ssh_host_rsa_key.pub";
        "ssh/ssh_host_ed25519_key".source = "/mnt/persist/etc/ssh/ssh_host_ed25519_key";
        "ssh/ssh_host_ed25519_key.pub".source = "/mnt/persist/etc/ssh/ssh_host_ed25519_key.pub";
        "machine-id".source = "/mnt/persist/etc/machine-id";
      };

      fileSystems = {
        "/" = cfg.paranoia.rootMount;
        # NOTE: Root tmpfs does not work yet
        #"/" = {
        #  device = "tmpfs";
        #  fsType = "tmpfs";
        #  options = [ "size=2G" "mode=755" "noexec" ];
        #};
        "/mnt/persist" = mkMerge [
          cfg.paranoia.persistMount
          {
            neededForBoot = true;
          }
        ];
        # for bios boot
        # TODO: uefi boot
        "/boot" = {
          depends = [ "/mnt/persist" ];
          device = "/mnt/persist/boot";
          fsType = "none";
          options = [ "bind" ];
        };
        "/nix" = {
          depends = [ "/mnt/persist" ];
          device = "/mnt/persist/nix";
          fsType = "none";
          options = [ "bind" ];
        };
        "/etc/nixos" = {
          depends = [ "/mnt/persist" ];
          device = "/mnt/persist/etc/nixos";
          fsType = "none";
          options = [ "bind" "noexec" ];
        };
        "/srv" = {
          depends = [ "/mnt/persist" ];
          device = "/mnt/persist/srv";
          fsType = "none";
          options = [ "bind" "noexec" ];
        };
        "/var/log" = {
          depends = [ "/mnt/persist" ];
          device = "/mnt/persist/var/log";
          fsType = "none";
          options = [ "bind" "noexec" ];
        };
        "/var/lib" = {
          depends = [ "/mnt/persist" ];
          device = "/mnt/persist/var/lib";
          fsType = "none";
          options = [ "bind" ]; # noexec?
        };
      };

    };
  }
